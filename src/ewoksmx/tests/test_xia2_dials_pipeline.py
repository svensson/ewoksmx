import os
import pathlib

from ewoksmx.tasks.XIA2_DIALS_pipeline import XIA2_DIALS_pipeline

from ewoksmx.lib.XSDataControlXia2DIALSv1_0 import XSDataInputControlXia2DIALS


def test_create_input_xml():
    metadata = {
        "MX_dataCollectionId": 123456,
        "forced_spacegroup": "P1",
        "anomalous": True,
        "low_res_limit": 4.0,
        "high_res_limit": 1.0,
        "start_image": 10,
        "end_image": 990,
        "exclude_range": [[10, 20], [30, 40]],
    }
    xia2_dials_pipeline = XIA2_DIALS_pipeline(
        inputs={"metadata": metadata, "XIA2_DIALS": True}
    )
    ednaproc_input_xml = xia2_dials_pipeline.create_input_xml(metadata, "/tmp")
    xsData = XSDataInputControlXia2DIALS.parseString(ednaproc_input_xml)
    assert xsData.dataCollectionId.value == 123456
    assert xsData.spaceGroup.value == "P1"
    assert xsData.doAnom
    assert xsData.icatProcessDataDir.path.value == "/tmp"
    assert xsData.startFrame.value == 10
    assert xsData.endFrame.value == 990
    assert xsData.reprocess
    assert xsData.exclude_range[0].begin == 10
    assert xsData.exclude_range[0].end == 20
    assert xsData.exclude_range[1].begin == 30
    assert xsData.exclude_range[1].end == 40


def test_create_edna_launch_script(tmpdir):
    pipeline_name = "auto_PROC"
    edna_plugin_name = "EDPluginControlAutoPROCv1_0"
    icat_dir = pathlib.Path(tmpdir)
    metadata = {
        "beamline": "id30a2",
        "proposal": "mx2112",
        "MX_dataCollectionId": 123456,
        "reprocess_path": str(tmpdir),
    }
    xia2_dials_pipeline = XIA2_DIALS_pipeline(
        inputs={"metadata": metadata, "XIA2_DIALS": True}
    )
    script_file_path = xia2_dials_pipeline.create_edna_launch_script(
        metadata=xia2_dials_pipeline.inputs.metadata,
        pipeline_name=pipeline_name,
        edna_plugin_name=edna_plugin_name,
        icat_dir=icat_dir,
    )
    assert os.path.exists(script_file_path)
