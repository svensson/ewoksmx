import json

from ewoksmx.tasks.MXPipelineInput import MXPipelineInput


def test_run(tmpdir):
    test_raw_data_path = tmpdir.mkdir("RAW_DATA")
    metadata = {"MX_dataCollectionId": 123456, "MX_template": "template"}
    metadata_path = test_raw_data_path / "metadata.json"
    with open(metadata_path, "w") as f:
        f.write(json.dumps(metadata, indent=4))

    test_processed_data_path = tmpdir.mkdir("PROCESSED_DATA")
    test_autoproc_dir = test_processed_data_path.mkdir("autoproc")
    xds_path = test_autoproc_dir / "XDS.INP"
    with open(xds_path, "w") as f:
        f.write("XDS INP\n")

    mxpipeline_input = MXPipelineInput(
        inputs={
            "raw_data_path": [str(test_raw_data_path)],
            "callback": None,
            "mx_pipeline_name": [
                "EDNA_proc",
                "autoPROC",
                "XIA2_DIALS",
                "grenades_fastproc",
                "grenades_parallelproc",
            ],
            "beamline": "id30a-1",
            "icat_metadata": None,
            "forced_spacegroup": "P1",
            "forced_cell": None,
            "start_image": 10,
            "end_image": 990,
            "anomalous": False,
            "low_res_limit": 4.0,
            "high_res_limit": 1.0,
            "exclude": "10-20, 30-40",
        }
    )
    mxpipeline_input.run()
    output_values = mxpipeline_input.get_output_values()
    assert output_values["EDNA_proc"]
    assert output_values["XIA2_DIALS"]
    assert output_values["autoPROC"]
    assert output_values["grenades_fastproc"]
    assert output_values["grenades_parallelproc"]
    metadata = output_values["metadata"]
    assert metadata["MX_dataCollectionId"] == 123456
    assert metadata["MX_template"] == "template"
    assert not metadata["anomalous"]
    assert metadata["beamline"] == "unknown"
    assert metadata["proposal"] == "unknown"
    assert metadata["start_image"] == 10
    assert metadata["end_image"] == 990
    assert metadata["forced_cell"] is None
    assert metadata["forced_spacegroup"] == "P1"
    assert metadata["high_res_limit"] == 1.0
    assert metadata["icat_callback_url"] is None
    assert metadata["job_id"] is None
    assert metadata["low_res_limit"] == 4.0
    assert metadata["no_pipelines"] == 5
    assert metadata["reprocess_path"] == str(
        test_processed_data_path / "reprocess_template_run_1"
    )
    assert metadata["xds_inp_path"] == str(xds_path)
    assert metadata["exclude_range"] == [[10, 20], [30, 40]]
