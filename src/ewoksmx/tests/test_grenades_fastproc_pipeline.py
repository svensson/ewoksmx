import os
import pathlib
import unittest

from ewoksmx.tasks.Grenades_fastproc_pipeline import Grenades_fastproc_pipeline

XDS_INP = """
! ispyb web service request time: 0.23s
! generated Tue, 12 Dec 2023 15:29:58
! params: raw_data=True, basedir=../links, request_basedir=../links

   JOB= ALL !XYCORR INIT COLSPOT IDXREF DEFPIX XPLAN INTEGRATE CORRECT
   !JOB= DEFPIX XPLAN INTEGRATE CORRECT
   DATA_RANGE= 1 1000
   
   SPOT_RANGE= 1 50
   
   SPOT_RANGE= 476 525
   
   SPOT_RANGE= 951 1000
   
   
   BACKGROUND_RANGE= 1 10
   

   !EXCLUSION OF HORIZONTAL DEAD AREAS OF THE EIGER 9M DETECTOR
   UNTRUSTED_RECTANGLE=    0 3108    512  549
   UNTRUSTED_RECTANGLE=    0 3108   1062 1099
   UNTRUSTED_RECTANGLE=    0 3108   1612 1649
   UNTRUSTED_RECTANGLE=    0 3108   2162 2199
   UNTRUSTED_RECTANGLE=    0 3108   2712 2749
 
   !EXCLUSION OF VERTICAL DEAD PIXELS OF THE EIGER 9M DETECTOR
   UNTRUSTED_RECTANGLE=    1028 1039   0  3262
   UNTRUSTED_RECTANGLE=    2068 2079   0  3262

   !Exclusion of beamstop
!   UNTRUSTED_RECTANGLE=    1550 1590   1665 3262


    !EXCLUSION OF DEAD AREAS OF THE EIGER 9M DETECTOR
   ! UNTRUSTED_RECTANGLE=    0 1035   1100 1355



   TRUSTED_REGION=0.0 1.41 !Relative radii limiting trusted detector region

   SECONDS=300
   MINIMUM_NUMBER_OF_PIXELS_IN_A_SPOT= 2

   !STRONG_PIXEL= 3.0

   OSCILLATION_RANGE= 0.2000
   STARTING_ANGLE= 200.0
   STARTING_FRAME= 1
   X-RAY_WAVELENGTH=  0.87313
   LIB= /opt/pxsoft/xds/v20220110/XDS-INTEL64_Linux_x86_64/dectris-neggia.so
   NAME_TEMPLATE_OF_DATA_FRAMES= /data/id23eh2/inhouse/opid232/20231212/RAW_DATA/Sample-8-2-05/phylsepSe_hel_low_1_1_??????.h5 ! H5

   !STARTING_ANGLES_OF_SPINDLE_ROTATION= 0 180 10
   !TOTAL_SPINDLE_ROTATION_RANGES= 60 180 10

   DETECTOR_DISTANCE= 377.24
   DETECTOR=PILATUS         MINIMUM_VALID_PIXEL_VALUE=0  OVERLOAD=1048500

   SENSOR_THICKNESS=0.45
   ORGX= 1595.61 ORGY= 1611.48
   NX=3108   NY=3262
   QX= 0.0750  QY= 0.0750
   VALUE_RANGE_FOR_TRUSTED_DETECTOR_PIXELS= 5000 30000

   DIRECTION_OF_DETECTOR_X-AXIS= 1.0 0.0 0.0
   DIRECTION_OF_DETECTOR_Y-AXIS= 0.0 1.0 0.0
   ROTATION_AXIS= 0.0 -1.0 0.0                             ! maybe make negative?
   INCIDENT_BEAM_DIRECTION= 0.0 0.0 1.0
   FRACTION_OF_POLARIZATION= 0.99
   POLARIZATION_PLANE_NORMAL= 0.0 1.0 0.0

   SPACE_GROUP_NUMBER= 0
   UNIT_CELL_CONSTANTS= 0 0 0 0 0 0
   INCLUDE_RESOLUTION_RANGE= 50.0 0.0
   !RESOLUTION_SHELLS= 15.0 8.0 4.0 2.8 2.4
   !FRIEDEL'S_LAW= FALSE !default is TRUE
   !STRICT_ABSORPTION_CORRECTION=TRUE

   REFINE(INTEGRATE)= BEAM ORIENTATION CELL
   !== Default value recommended
   !DELPHI= %.3f
   MAXIMUM_NUMBER_OF_PROCESSORS= 16
   MAXIMUM_NUMBER_OF_JOBS= 1

   SEPMIN= 3
   CLUSTER_RADIUS= 1
FRIEDEL'S_LAW=FALSE
SPOT_RANGE=441 451

SPOT_RANGE=216 226
"""  # noqa W293


def test_create_xds_inp(tmpdir):
    # Create dummy XDS.INP file
    grenades_working_dir = tmpdir / "grenades_fastproc"
    grenades_working_dir.mkdir()
    xds_inp_path = tmpdir / "XDS.INP"
    with open(xds_inp_path, "w") as f:
        f.write(XDS_INP)
    metadata = {
        "MX_dataCollectionId": 123456,
        "MX_directory": "/data/visitor/mx2112/20231224/RAW_DATA",
        "forced_spacegroup": "P1",
        "xds_inp_path": str(xds_inp_path),
        "exclude_range": [[10, 20], [30, 40]],
    }
    grenades_fastproc_pipeline = Grenades_fastproc_pipeline(
        inputs={"metadata": metadata, "grenades_fastproc": True}
    )
    directory = tmpdir
    xds_inp_path = grenades_fastproc_pipeline.create_xds_inp(
        directory=directory,
        xds_inp_path=xds_inp_path,
        grenades_working_dir=grenades_working_dir,
    )
    _ = grenades_fastproc_pipeline.modify_xds_inp(
        grenades_working_dir=grenades_working_dir,
        exclude_range=metadata["exclude_range"],
    )
    assert xds_inp_path.exists()
    with open(xds_inp_path) as f:
        list_xds_inp = f.readlines()
    found_name_template = False
    list_exclude_range = []
    for line in list_xds_inp:
        if "NAME_TEMPLATE_OF_DATA_FRAMES" in line:
            name_template = line.split("=")[1].strip()
            assert os.path.dirname(name_template) == str(tmpdir)
            found_name_template = True
        if "EXCLUDE_DATA_RANGE" in line:
            list_exclude_range.append(line.split("=")[1].split())
    assert found_name_template
    assert len(list_exclude_range) == 2
    assert list_exclude_range[0] == ["10", "20"]
    assert list_exclude_range[1] == ["30", "40"]


# def test_get_cell_params(tmpdir):
@unittest.skipIf(
    not os.path.exists(
        "/opt/pxsoft/adp/max/vdefault/debian90/cell_from_space_group.pl"
    ),
    "Missing cell_from_space_group.pl executable",
)
@unittest.skipIf(
    not os.path.exists(
        "/data/id23eh2/inhouse/opid232/20231212/RAW_DATA/Sample-8-2-05/phylsepSe_hel_low_1_1_master.h5"
    ),
    "Missing test data",
)
def test_get_spg_and_cell_params(tmpdir):
    grenades_working_dir = tmpdir / "grenades_fastproc"
    grenades_working_dir.mkdir()
    # Create dummy XDS.INP file
    xds_inp_path = grenades_working_dir / "XDS.INP"
    with open(xds_inp_path, "w") as f:
        f.write(XDS_INP)
    metadata = {
        "MX_dataCollectionId": 123456,
        "MX_directory": "/data/visitor/mx2112/20231224/RAW_DATA",
        "forced_spacegroup": "P1",
        "xds_inp_path": str(xds_inp_path),
    }
    grenades_fastproc_pipeline = Grenades_fastproc_pipeline(
        inputs={"metadata": metadata, "grenades_fastproc": True}
    )
    spg_number, cell_params = grenades_fastproc_pipeline.get_spg_and_cell_params(
        grenades_working_dir=grenades_working_dir,
        space_group_name="P1",
    )
    assert spg_number == 1
    assert cell_params == [77.5, 155.8, 155.9, 60.1, 89.9, 89.8]


def test_modify_xds_inp(tmpdir):
    # Create dummy XDS.INP file
    grenades_working_dir = tmpdir / "grenades_fastproc"
    grenades_working_dir.mkdir()
    xds_inp_path = grenades_working_dir / "XDS.INP"
    with open(xds_inp_path, "w") as f:
        f.write(XDS_INP)
    metadata = {
        "MX_dataCollectionId": 123456,
        "MX_directory": "/data/visitor/mx2112/20231224/RAW_DATA",
        "forced_spacegroup": "P1",
        "xds_inp_path": str(xds_inp_path),
    }
    grenades_fastproc_pipeline = Grenades_fastproc_pipeline(
        inputs={"metadata": metadata, "grenades_fastproc": True}
    )
    space_group_number = 1
    cell_params = [77.5, 155.8, 155.9, 60.1, 89.9, 89.8]
    xds_inp_path = grenades_fastproc_pipeline.modify_xds_inp(
        grenades_working_dir=grenades_working_dir,
        space_group_number=space_group_number,
        cell_params=cell_params,
    )
    assert xds_inp_path.exists()
    with open(xds_inp_path) as f:
        list_xds_inp = f.readlines()
    found_space_group_number = False
    for line in list_xds_inp:
        if "SPACE_GROUP_NUMBER" in line:
            space_group_number = int(line.split("=")[1])
            assert space_group_number == 1
            found_space_group_number = True
            break
    assert found_space_group_number


def test_create_dcolid_file(tmpdir):
    metadata = {"MX_dataCollectionId": 123456}
    grenades_working_dir = pathlib.Path(tmpdir) / "grenades_fastproc"
    grenades_working_dir.mkdir()
    grenades_fastproc_pipeline = Grenades_fastproc_pipeline(
        inputs={"metadata": metadata, "grenades_fastproc": True}
    )
    dcolid_path = grenades_fastproc_pipeline.create_dcolid_file(
        metadata=metadata, grenades_working_dir=grenades_working_dir
    )
    assert dcolid_path.exists()
    with open(dcolid_path) as f:
        dcolid = f.read()
    assert dcolid == "datacollectionID:123456\n"


def test_create_grenades_fastproc_script(tmpdir):
    metadata = {"MX_dataCollectionId": 123456}
    grenades_working_dir = tmpdir / "grenades_fastproc"
    grenades_working_dir.mkdir()
    grenades_fastproc_pipeline = Grenades_fastproc_pipeline(
        inputs={"metadata": metadata, "grenades_fastproc": True}
    )
    script_file_path = grenades_fastproc_pipeline.create_grenades_fastproc_script(
        metadata=metadata,
        grenades_top_dir=tmpdir,
        grenades_working_dir=grenades_working_dir,
    )
    assert script_file_path.exists()
    # with open(script_file_path) as f:
    #     script_file = f.read()
    # print(script_file)
