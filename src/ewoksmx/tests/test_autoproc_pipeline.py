import os
import pathlib

from ewoksmx.tasks.AutoPROC_pipeline import AutoPROC_pipeline

from ewoksmx.lib.XSDataControlAutoPROCv1_0 import XSDataInputControlAutoPROC


def test_create_input_xml():
    metadata = {
        "MX_dataCollectionId": 123456,
        "forced_spacegroup": "P1",
        "anomalous": True,
        "low_res_limit": 4.0,
        "high_res_limit": 1.0,
        "start_image": 10,
        "end_image": 990,
        "exclude_range": [[10, 20], [30, 40]],
    }
    autoproc_pipeline = AutoPROC_pipeline(
        inputs={"metadata": metadata, "autoPROC": True}
    )
    auto_proc_input_xml = autoproc_pipeline.create_input_xml(metadata, "/tmp")
    xsData = XSDataInputControlAutoPROC.parseString(auto_proc_input_xml)
    assert xsData.dataCollectionId.value == 123456
    assert xsData.symm.value == "P1"
    assert xsData.lowResolutionLimit.value == 4.0
    assert xsData.highResolutionLimit.value == 1.0
    assert xsData.doAnom
    assert xsData.icatProcessDataDir.path.value == "/tmp"
    assert xsData.fromN.value == 10
    assert xsData.toN.value == 990
    assert xsData.reprocess
    assert xsData.exclude_range[0].begin == 10
    assert xsData.exclude_range[0].end == 20
    assert xsData.exclude_range[1].begin == 30
    assert xsData.exclude_range[1].end == 40


def test_create_edna_launch_script(tmpdir):
    pipeline_name = "auto_PROC"
    edna_plugin_name = "EDPluginControlAutoPROCv1_0"
    icat_dir = pathlib.Path(tmpdir)
    metadata = {
        "beamline": "id30a2",
        "proposal": "mx2112",
        "MX_dataCollectionId": 123456,
        "reprocess_path": str(tmpdir),
    }
    autoproc_pipeline = AutoPROC_pipeline(
        inputs={"metadata": metadata, "autoPROC": True}
    )
    script_file_path = autoproc_pipeline.create_edna_launch_script(
        metadata=autoproc_pipeline.inputs.metadata,
        pipeline_name=pipeline_name,
        edna_plugin_name=edna_plugin_name,
        icat_dir=icat_dir,
    )
    assert os.path.exists(script_file_path)
