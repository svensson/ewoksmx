import os
import pathlib

from ewoksmx.tasks.EDNA_proc_pipeline import EDNA_proc_pipeline

from ewoksmx.lib.XSDataEDNAprocv1_0 import XSDataEDNAprocInput


def test_create_input_xml():
    metadata = {
        "MX_dataCollectionId": 123456,
        "forced_spacegroup": "P1",
        "anomalous": True,
        "low_res_limit": 4.0,
        "high_res_limit": 1.0,
        "start_image": 10,
        "end_image": 990,
        "xds_inp_path": "/tmp/XDS.INP",
    }
    ednaproc_pipeline = EDNA_proc_pipeline(
        inputs={"metadata": metadata, "EDNA_proc": True}
    )
    ednaproc_input_xml = ednaproc_pipeline.create_input_xml(metadata, "/tmp")
    xsData = XSDataEDNAprocInput.parseString(ednaproc_input_xml)
    assert xsData.data_collection_id.value == 123456
    assert xsData.spacegroup.value == "P1"
    assert xsData.res_override.value == 1.0
    assert xsData.doAnom
    assert xsData.icat_processed_data_dir.value == "/tmp"
    assert xsData.start_image.value == 10
    assert xsData.end_image.value == 990
    assert xsData.reprocess
    assert xsData.input_file.path.value == "/tmp/XDS.INP"
    assert xsData.exclude_range == []


def test_create_input_xml_with_exclude_range():
    metadata = {
        "MX_dataCollectionId": 123456,
        "forced_spacegroup": "P1",
        "anomalous": True,
        "low_res_limit": 4.0,
        "high_res_limit": 1.0,
        "start_image": 10,
        "end_image": 990,
        "xds_inp_path": "/tmp/XDS.INP",
        "exclude_range": [[10, 20], [30, 40]],
    }
    ednaproc_pipeline = EDNA_proc_pipeline(
        inputs={"metadata": metadata, "EDNA_proc": True}
    )
    ednaproc_input_xml = ednaproc_pipeline.create_input_xml(metadata, "/tmp")
    xsData = XSDataEDNAprocInput.parseString(ednaproc_input_xml)
    assert xsData.data_collection_id.value == 123456
    assert xsData.spacegroup.value == "P1"
    assert xsData.res_override.value == 1.0
    assert xsData.doAnom
    assert xsData.icat_processed_data_dir.value == "/tmp"
    assert xsData.start_image.value == 10
    assert xsData.end_image.value == 990
    assert xsData.reprocess
    assert xsData.input_file.path.value == "/tmp/XDS.INP"
    assert xsData.exclude_range[0].begin == 10
    assert xsData.exclude_range[0].end == 20
    assert xsData.exclude_range[1].begin == 30
    assert xsData.exclude_range[1].end == 40


def test_create_edna_launch_script(tmpdir):
    pipeline_name = "EDNA_proc"
    edna_plugin_name = "EDPluginControlEDNAprocv1_0"
    icat_dir = pathlib.Path(tmpdir)
    metadata = {
        "beamline": "id30a2",
        "proposal": "mx2112",
        "MX_dataCollectionId": 123456,
        "reprocess_path": str(tmpdir),
        "xds_inp_path": "/tmp/XDS.INP",
        "exclude_range": [[10, 20], [30, 40]],
    }
    ednaproc_pipeline = EDNA_proc_pipeline(
        inputs={"metadata": metadata, "EDNA_proc": True}
    )
    script_file_path = ednaproc_pipeline.create_edna_launch_script(
        metadata=ednaproc_pipeline.inputs.metadata,
        pipeline_name=pipeline_name,
        edna_plugin_name=edna_plugin_name,
        icat_dir=icat_dir,
    )
    assert os.path.exists(script_file_path)
