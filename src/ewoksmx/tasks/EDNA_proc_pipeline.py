from ewoksmx.tasks.Abstract_pipeline import Abstract_pipeline

from ewoksmx.lib.XSDataCommon import XSDataFile
from ewoksmx.lib.XSDataCommon import XSDataString
from ewoksmx.lib.XSDataCommon import XSDataDouble
from ewoksmx.lib.XSDataCommon import XSDataBoolean
from ewoksmx.lib.XSDataCommon import XSDataInteger

from ewoksmx.lib.XSDataEDNAprocv1_0 import XSDataRange
from ewoksmx.lib.XSDataEDNAprocv1_0 import XSDataEDNAprocInput


class EDNA_proc_pipeline(
    Abstract_pipeline,
    input_names=["metadata", "EDNA_proc"],
    output_names=["pipeline_name", "slurm_params"],
):
    def create_input_xml(self, metadata, icat_dir):
        xsDataEDNAprocInput = XSDataEDNAprocInput()
        xsDataEDNAprocInput.reprocess = XSDataBoolean(True)
        xsDataEDNAprocInput.data_collection_id = XSDataInteger(
            metadata["MX_dataCollectionId"]
        )
        xsDataEDNAprocInput.input_file = XSDataFile(
            XSDataString(metadata["xds_inp_path"])
        )
        xsDataEDNAprocInput.icat_processed_data_dir = XSDataString(str(icat_dir))
        if (
            "forced_spacegroup" in metadata
            and metadata["forced_spacegroup"] is not None
        ):
            xsDataEDNAprocInput.spacegroup = XSDataString(metadata["forced_spacegroup"])
        if "anomalous" in metadata and metadata["anomalous"] is not None:
            xsDataEDNAprocInput.doAnom = XSDataBoolean(metadata["anomalous"])
        if "high_res_limit" in metadata and metadata["high_res_limit"] is not None:
            xsDataEDNAprocInput.res_override = XSDataDouble(metadata["high_res_limit"])
        if "start_image" in metadata and metadata["start_image"] is not None:
            xsDataEDNAprocInput.start_image = XSDataInteger(metadata["start_image"])
        if "end_image" in metadata and metadata["end_image"] is not None:
            xsDataEDNAprocInput.end_image = XSDataInteger(metadata["end_image"])
        for begin, end in metadata.get("exclude_range", ()):
            xsDataRange = XSDataRange()
            xsDataRange.begin = begin
            xsDataRange.end = end
            xsDataEDNAprocInput.exclude_range.append(xsDataRange)
        return xsDataEDNAprocInput.marshal()

    def run(self):
        pipeline_name = "EDNA_proc"
        edna_plugin_name = "EDPluginControlEDNAprocv1_0"
        metadata = self.inputs.metadata

        icat_dir = self.create_icat_dir(metadata, pipeline_name)

        script_file_path = self.create_edna_launch_script(
            metadata=metadata,
            pipeline_name=pipeline_name,
            edna_plugin_name=edna_plugin_name,
            icat_dir=icat_dir,
        )

        slurm_params = {
            "script_file_path": script_file_path,
            "queue": "mx",
            "mem": 16000,
            "nodes": 1,
            "core": 20,
            "time": "2:00:00",
            "icat_dir": str(icat_dir),
            "icat_callback_url": self.inputs.metadata["icat_callback_url"],
            "no_pipelines": self.inputs.metadata["no_pipelines"],
        }
        self.outputs.pipeline_name = pipeline_name
        self.outputs.slurm_params = slurm_params
