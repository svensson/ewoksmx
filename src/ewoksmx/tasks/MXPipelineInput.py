import json
import pathlib
import requests

from ewokscore import Task

from edna2.utils import UtilsPath


class MXPipelineInput(
    Task,
    input_names=["raw_data_path"],
    optional_input_names=[
        "mx_pipeline_name",
        "beamline",
        "icat_metadata",
        "forced_spacegroup",
        "forced_cell",
        "start_image",
        "end_image",
        "anomalous",
        "low_res_limit",
        "high_res_limit",
        "callback",
        "exclude",
    ],
    output_names=[
        "metadata",
        "EDNA_proc",
        "autoPROC",
        "XIA2_DIALS",
        "grenades_fastproc",
        "grenades_parallelproc",
    ],
):
    def run(self):
        raw_data_path = pathlib.Path(self.inputs.raw_data_path[0])
        if not raw_data_path.exists():
            raise FileNotFoundError(str(raw_data_path))
        # Load metadata
        metadata = self.get_input_value("icat_metadata", None)
        if metadata is None:
            metadata_path = raw_data_path / "metadata.json"
            if metadata_path.exists():
                with open(metadata_path) as f:
                    metadata = json.loads(f.read())
            else:
                raise FileNotFoundError(str(metadata_path))
        # Callback
        metadata["icat_callback_url"] = self.inputs.callback
        if metadata["icat_callback_url"] is not None:
            requests.put(self.inputs.callback, json={"status": "RUNNING"})
        # Get beamline and proposal
        beamline, proposal = UtilsPath.getBeamlineProposal(str(raw_data_path))
        metadata["beamline"] = beamline
        metadata["proposal"] = proposal
        # Find the XDS.INP file (if present)
        processed_data_path = pathlib.Path(
            str(raw_data_path).replace("RAW_DATA", "PROCESSED_DATA")
        )
        list_xds_path = list(processed_data_path.glob("auto*/XDS.INP"))
        mx_pipeline_name = self.get_input_value("mx_pipeline_name", ["EDNA_proc"])
        metadata["no_pipelines"] = len(mx_pipeline_name)
        if list_xds_path is None or len(list_xds_path) == 0:
            raise RuntimeError(
                f"Missing XDS path, processed_data_path: {processed_data_path} and list_xds_path: {list_xds_path}"
            )
        metadata["xds_inp_path"] = str(list_xds_path[0])
        # Set up processing path
        run = 1
        template = metadata["MX_template"]
        prefix = template.split("_%")[0]
        do_continue = True
        while do_continue:
            reprocess_path = processed_data_path / f"reprocess_{prefix}_run_{run}"
            if reprocess_path.exists():
                run += 1
            else:
                try:
                    reprocess_path.mkdir(parents=True, exist_ok=False, mode=0o755)
                    do_continue = False
                except FileExistsError:
                    do_continue = True
        metadata["reprocess_path"] = str(reprocess_path)
        dcolid_path = reprocess_path / "DCOLID.txt"
        with open(dcolid_path, "w") as f:
            f.write(f"datacollectionID:{metadata['MX_dataCollectionId']}\n")
        # Raise error if no metadata at this point
        if metadata is None:
            raise RuntimeError("No metadata provided!")
        metadata["forced_spacegroup"] = self.get_input_value("forced_spacegroup", None)
        metadata["forced_cell"] = self.get_input_value("forced_cell", None)
        metadata["start_image"] = self.get_input_value("start_image", None)
        metadata["end_image"] = self.get_input_value("end_image", None)
        metadata["anomalous"] = self.get_input_value("anomalous", True)
        metadata["low_res_limit"] = self.get_input_value("low_res_limit", None)
        metadata["high_res_limit"] = self.get_input_value("high_res_limit", None)
        # Parse exclude ranges and convert to list of list of integers
        exclude = self.get_input_value("exclude", None)
        if exclude is not None:
            exclude_range = []
            for str_range in exclude.split(","):
                str_begin, str_end = str_range.split("-")
                exclude_range.append([int(str_begin), int(str_end)])
            metadata["exclude_range"] = exclude_range
        metadata["job_id"] = self.job_id
        self.outputs.metadata = metadata
        self.outputs.EDNA_proc = "EDNA_proc" in mx_pipeline_name
        self.outputs.autoPROC = "autoPROC" in mx_pipeline_name
        self.outputs.XIA2_DIALS = "XIA2_DIALS" in mx_pipeline_name
        self.outputs.grenades_fastproc = "grenades_fastproc" in mx_pipeline_name
        self.outputs.grenades_parallelproc = "grenades_parallelproc" in mx_pipeline_name
