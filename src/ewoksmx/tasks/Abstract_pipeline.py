import os
import string
import pathlib

from ewokscore import Task
from edna2.utils import UtilsSlurm

TASK_PATH = pathlib.Path(__file__)
MODULE_DIR = TASK_PATH.parents[1]
TEMPLATE_DIR = MODULE_DIR / "template"


class Abstract_pipeline(
    Task,
    input_names=[],
    output_names=[],
):
    def create_icat_dir(self, metadata, pipeline_name):
        icat_dir = pathlib.Path(metadata["reprocess_path"]) / pipeline_name
        return icat_dir

    def create_edna_launch_script(
        self, metadata, pipeline_name, edna_plugin_name, icat_dir
    ):
        edna_base_dir = icat_dir / "nobackup"
        edna_base_dir.mkdir(parents=True, exist_ok=False, mode=0o755)
        input_file_path = edna_base_dir / f"{pipeline_name}_input.xml"
        input_xml = self.create_input_xml(metadata, icat_dir)
        with open(input_file_path, "w") as f:
            f.write(input_xml)
        path_to_template = os.path.join(TEMPLATE_DIR, "edna_script.template")
        with open(path_to_template) as f:
            scriptTemplate = f.read()
        template = string.Template(scriptTemplate)
        script = template.substitute(
            beamline=metadata["beamline"],
            proposal=metadata["proposal"],
            scriptDir=edna_base_dir,
            dataCollectionId=metadata["MX_dataCollectionId"],
            inputFile=input_file_path,
            ispybUserName=os.environ.get("ISPyB_user", None),
            ispybPassword=os.environ.get("ISPyB_pass", None),
            EDNA_SITE=os.environ.get("EDNA_SITE", None),
            pluginName=edna_plugin_name,
            name=pipeline_name,
            residues=200,
            workingDir=edna_base_dir,
        )
        script_file_name = f"{pipeline_name}_launcher.sh"
        script_file_path = os.path.join(edna_base_dir, script_file_name)
        with open(script_file_path, "w") as f:
            f.write(script)
        os.chmod(script_file_path, 0o755)
        return script_file_path

    def submit_slurm_job(
        self,
        pipeline_name,
        script_file_path,
        queue="mx",
        mem=8000,
        nodes=1,
        core=8,
        time="2:00:00",
        environ=None,
    ):
        command_line = ""
        if environ is not None:
            for key, value in environ.items():
                command_line += f"export {key}={value}; "
        command_line += script_file_path
        working_directory = os.path.dirname(script_file_path)
        slurm_script_path, slurm_id, stdout, stderr = UtilsSlurm.submit_job_to_slurm(
            command_line=command_line,
            working_directory=working_directory,
            queue=queue,
            mem=mem,
            nodes=nodes,
            core=core,
            time=time,
            name=pipeline_name,
        )
        return slurm_script_path, slurm_id, stdout, stderr
