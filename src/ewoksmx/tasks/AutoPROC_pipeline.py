from ewoksmx.tasks.Abstract_pipeline import Abstract_pipeline

from ewoksmx.lib.XSDataCommon import XSDataFile
from ewoksmx.lib.XSDataCommon import XSDataString
from ewoksmx.lib.XSDataCommon import XSDataDouble
from ewoksmx.lib.XSDataCommon import XSDataBoolean
from ewoksmx.lib.XSDataCommon import XSDataInteger
from ewoksmx.lib.XSDataCommon import XSDataRange

from ewoksmx.lib.XSDataControlAutoPROCv1_0 import XSDataInputControlAutoPROC


class AutoPROC_pipeline(
    Abstract_pipeline,
    input_names=["metadata", "autoPROC"],
    output_names=["pipeline_name", "slurm_params"],
):
    def create_input_xml(self, metadata, icat_dir):
        xsDataInputControlAutoPROC = XSDataInputControlAutoPROC()
        xsDataInputControlAutoPROC.reprocess = XSDataBoolean(True)
        xsDataInputControlAutoPROC.dataCollectionId = XSDataInteger(
            metadata["MX_dataCollectionId"]
        )
        xsDataInputControlAutoPROC.icatProcessDataDir = XSDataFile(
            XSDataString(str(icat_dir))
        )
        if (
            "forced_spacegroup" in metadata
            and metadata["forced_spacegroup"] is not None
        ):
            xsDataInputControlAutoPROC.symm = XSDataString(
                metadata["forced_spacegroup"]
            )
        if "anomalous" in metadata and metadata["anomalous"] is not None:
            xsDataInputControlAutoPROC.doAnom = XSDataBoolean(metadata["anomalous"])
        if "low_res_limit" in metadata and metadata["low_res_limit"] is not None:
            xsDataInputControlAutoPROC.lowResolutionLimit = XSDataDouble(
                metadata["low_res_limit"]
            )
        if "high_res_limit" in metadata and metadata["high_res_limit"] is not None:
            xsDataInputControlAutoPROC.highResolutionLimit = XSDataDouble(
                metadata["high_res_limit"]
            )
        if "start_image" in metadata and metadata["start_image"] is not None:
            xsDataInputControlAutoPROC.fromN = XSDataInteger(metadata["start_image"])
        if "end_image" in metadata and metadata["end_image"] is not None:
            xsDataInputControlAutoPROC.toN = XSDataInteger(metadata["end_image"])
        for begin, end in metadata.get("exclude_range", ()):
            xsDataRange = XSDataRange()
            xsDataRange.begin = begin
            xsDataRange.end = end
            xsDataInputControlAutoPROC.exclude_range.append(xsDataRange)
        return xsDataInputControlAutoPROC.marshal()

    def run(self):
        pipeline_name = "autoPROC"
        edna_plugin_name = "EDPluginControlAutoPROCv1_0"
        metadata = self.inputs.metadata

        icat_dir = self.create_icat_dir(metadata, pipeline_name)

        script_file_path = self.create_edna_launch_script(
            metadata=metadata,
            pipeline_name=pipeline_name,
            edna_plugin_name=edna_plugin_name,
            icat_dir=icat_dir,
        )

        slurm_params = {
            "script_file_path": script_file_path,
            "queue": "mx",
            "mem": 16000,
            "nodes": 1,
            "core": 20,
            "time": "2:00:00",
            "icat_dir": str(icat_dir),
            "icat_callback_url": self.inputs.metadata["icat_callback_url"],
            "no_pipelines": self.inputs.metadata["no_pipelines"],
        }
        self.outputs.pipeline_name = pipeline_name
        self.outputs.slurm_params = slurm_params
