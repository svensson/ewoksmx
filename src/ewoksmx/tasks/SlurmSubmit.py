import os
import requests

from edna2.utils import UtilsSlurm

from ewokscore import Task


class SlurmSubmit(
    Task,
    input_names=["pipeline_name", "slurm_params"],
    output_names=[
        "slurm_params",
    ],
):
    def run(self):
        pipeline_name = self.inputs.pipeline_name
        slurm_params = self.inputs.slurm_params
        error_message = slurm_params.get("error_message", None)
        if error_message is None:
            icat_callback_url = slurm_params["icat_callback_url"]
            environ = slurm_params.get("environ", None)
            script_file_path = slurm_params["script_file_path"]
            command_line = ""
            if environ is not None:
                for key, value in environ.items():
                    command_line += f"export {key}={value}; "
            command_line += script_file_path
            working_directory = os.path.dirname(script_file_path)
            slurm_params["working_directory"] = working_directory
            (
                slurm_script_path,
                slurm_id,
                stdout,
                stderr,
            ) = UtilsSlurm.submit_job_to_slurm(
                command_line=command_line,
                working_directory=working_directory,
                queue=slurm_params["queue"],
                mem=slurm_params["mem"],
                nodes=slurm_params["nodes"],
                core=slurm_params["core"],
                time=slurm_params["time"],
                name=pipeline_name,
            )

            requests.put(
                icat_callback_url,
                json={
                    "step": {"name": pipeline_name, "status": "SUBMITTED"},
                },
            )

            requests.put(
                icat_callback_url,
                json={"logs": {"message": f"{pipeline_name} slurm id: {slurm_id}"}},
            )

            slurm_params["script_path"] = slurm_script_path
            slurm_params["slurm_id"] = slurm_id
            slurm_params["pipeline_name"] = pipeline_name

        self.outputs.slurm_params = slurm_params
